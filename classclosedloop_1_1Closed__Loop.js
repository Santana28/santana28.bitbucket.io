var classclosedloop_1_1Closed__Loop =
[
    [ "get_Kp", "classclosedloop_1_1Closed__Loop.html#a5eeaa49d95902576cbf34d09e8b196d0", null ],
    [ "init", "classclosedloop_1_1Closed__Loop.html#a300721fe3e4e7c728625aa075ec48c69", null ],
    [ "set_Kp", "classclosedloop_1_1Closed__Loop.html#ac01165e3390877a439bb6691ab5f40ea", null ],
    [ "update", "classclosedloop_1_1Closed__Loop.html#a53c329ab07cedeb3e3f3b7eea16707a0", null ],
    [ "max_lim", "classclosedloop_1_1Closed__Loop.html#aa4a2fbcca58f40227e59caa468ef2489", null ],
    [ "min_lim", "classclosedloop_1_1Closed__Loop.html#a7bad150e8c8205e9802eaebc13ed3f27", null ],
    [ "omega_measured", "classclosedloop_1_1Closed__Loop.html#a8b63a91abeb20ea7c37cf69ee654b184", null ],
    [ "omega_reference", "classclosedloop_1_1Closed__Loop.html#a035f3b63c8f4d195c951a0ac18525029", null ],
    [ "p_gain", "classclosedloop_1_1Closed__Loop.html#a5deab5848fe82b42bf2eb4c79baf6828", null ]
];