var files_dup =
[
    [ "closedloop.py", "closedloop_8py.html", [
      [ "closedloop.Closed_Loop", "classclosedloop_1_1Closed__Loop.html", "classclosedloop_1_1Closed__Loop" ]
    ] ],
    [ "encoder.py", "encoder_8py.html", [
      [ "encoder.Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "LED_patterns.py", "LED__patterns_8py.html", "LED__patterns_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "mainpage.py", "mainpage_8py.html", null ],
    [ "motor.py", "motor_8py.html", [
      [ "motor.DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "motor.Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_encoder.py", "task__encoder_8py.html", [
      [ "task_encoder.Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "TP_closedloop.py", "TP__closedloop_8py.html", [
      [ "TP_closedloop.TP_ClosedLoop", "classTP__closedloop_1_1TP__ClosedLoop.html", "classTP__closedloop_1_1TP__ClosedLoop" ]
    ] ],
    [ "TP_imu.py", "TP__imu_8py.html", [
      [ "TP_imu.TP_IMU", "classTP__imu_1_1TP__IMU.html", "classTP__imu_1_1TP__IMU" ]
    ] ],
    [ "TP_main.py", "TP__main_8py.html", null ],
    [ "TP_motor.py", "TP__motor_8py.html", [
      [ "TP_motor.TP_Motor", "classTP__motor_1_1TP__Motor.html", "classTP__motor_1_1TP__Motor" ]
    ] ],
    [ "TP_task_controller.py", "TP__task__controller_8py.html", [
      [ "TP_task_controller.TP_Task_Controller", "classTP__task__controller_1_1TP__Task__Controller.html", "classTP__task__controller_1_1TP__Task__Controller" ]
    ] ],
    [ "TP_task_data.py", "TP__task__data_8py.html", [
      [ "TP_task_data.TP_Task_Data", "classTP__task__data_1_1TP__Task__Data.html", "classTP__task__data_1_1TP__Task__Data" ]
    ] ],
    [ "TP_task_imu.py", "TP__task__imu_8py.html", [
      [ "TP_task_imu.TP_Task_IMU", "classTP__task__imu_1_1TP__Task__IMU.html", "classTP__task__imu_1_1TP__Task__IMU" ]
    ] ],
    [ "TP_task_motor.py", "TP__task__motor_8py.html", [
      [ "TP_task_motor.TP_Task_Motor", "classTP__task__motor_1_1TP__Task__Motor.html", "classTP__task__motor_1_1TP__Task__Motor" ]
    ] ],
    [ "TP_task_touchpanel.py", "TP__task__touchpanel_8py.html", [
      [ "TP_task_touchpanel.TP_Task_TouchPanel", "classTP__task__touchpanel_1_1TP__Task__TouchPanel.html", "classTP__task__touchpanel_1_1TP__Task__TouchPanel" ]
    ] ],
    [ "TP_task_user.py", "TP__task__user_8py.html", "TP__task__user_8py" ],
    [ "TP_touchpanel.py", "TP__touchpanel_8py.html", [
      [ "TP_touchpanel.TP_TouchPanel", "classTP__touchpanel_1_1TP__TouchPanel.html", "classTP__touchpanel_1_1TP__TouchPanel" ]
    ] ]
];