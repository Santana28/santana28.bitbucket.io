var classtask__motor_1_1Task__Motor =
[
    [ "__init__", "classtask__motor_1_1Task__Motor.html#ae761787c27552b8d082e6fbcb4c3a166", null ],
    [ "run", "classtask__motor_1_1Task__Motor.html#aa4c9789df3d5101e8d525e815ee2738a", null ],
    [ "closed_loop", "classtask__motor_1_1Task__Motor.html#a5bac128acdf32b88844f96743c9e337e", null ],
    [ "duty_share", "classtask__motor_1_1Task__Motor.html#a1e1b3dac5e3680c0f2e3fc4018ceb579", null ],
    [ "mot_flag", "classtask__motor_1_1Task__Motor.html#af559f54eee3ac9173cc894f650334e63", null ],
    [ "motor_1", "classtask__motor_1_1Task__Motor.html#a1622bf5def9a9d9b344a3b4c9fbca0bb", null ],
    [ "motor_2", "classtask__motor_1_1Task__Motor.html#a6292dff591b13f8e304ba60d169adae7", null ],
    [ "motor_drv", "classtask__motor_1_1Task__Motor.html#ab9fbc76ed185384fab3826dbba806aa1", null ],
    [ "next_time", "classtask__motor_1_1Task__Motor.html#a8cd5c18886c0439db6ad25900802e531", null ],
    [ "omega_share", "classtask__motor_1_1Task__Motor.html#ac6dce88e2086f49e0e26eae16799016c", null ],
    [ "omegameas1_share", "classtask__motor_1_1Task__Motor.html#aaa070651184151c0eb6007e7b068760d", null ],
    [ "omegameas2_share", "classtask__motor_1_1Task__Motor.html#ae3c634f59e54c479ada503964613a464", null ],
    [ "period", "classtask__motor_1_1Task__Motor.html#a083bab479ac16bea926548029165c0de", null ],
    [ "Pgain_share", "classtask__motor_1_1Task__Motor.html#aab12a7cc7c243679745ecbdd7ce3cd95", null ],
    [ "runs", "classtask__motor_1_1Task__Motor.html#a02858b7aeec0760954e0bc1ab28ac537", null ]
];