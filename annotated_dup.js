var annotated_dup =
[
    [ "closedloop", null, [
      [ "Closed_Loop", "classclosedloop_1_1Closed__Loop.html", "classclosedloop_1_1Closed__Loop" ]
    ] ],
    [ "encoder", null, [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "motor", null, [
      [ "DRV8847", "classmotor_1_1DRV8847.html", "classmotor_1_1DRV8847" ],
      [ "Motor", "classmotor_1_1Motor.html", "classmotor_1_1Motor" ]
    ] ],
    [ "shares", null, [
      [ "Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ],
      [ "Share", "classshares_1_1Share.html", "classshares_1_1Share" ]
    ] ],
    [ "task_encoder", null, [
      [ "Task_Encoder", "classtask__encoder_1_1Task__Encoder.html", "classtask__encoder_1_1Task__Encoder" ]
    ] ],
    [ "task_motor", null, [
      [ "Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_user", null, [
      [ "Task_User", "classtask__user_1_1Task__User.html", "classtask__user_1_1Task__User" ]
    ] ],
    [ "TP_closedloop", null, [
      [ "TP_ClosedLoop", "classTP__closedloop_1_1TP__ClosedLoop.html", "classTP__closedloop_1_1TP__ClosedLoop" ]
    ] ],
    [ "TP_imu", null, [
      [ "TP_IMU", "classTP__imu_1_1TP__IMU.html", "classTP__imu_1_1TP__IMU" ]
    ] ],
    [ "TP_motor", null, [
      [ "TP_Motor", "classTP__motor_1_1TP__Motor.html", "classTP__motor_1_1TP__Motor" ]
    ] ],
    [ "TP_task_controller", null, [
      [ "TP_Task_Controller", "classTP__task__controller_1_1TP__Task__Controller.html", "classTP__task__controller_1_1TP__Task__Controller" ]
    ] ],
    [ "TP_task_data", null, [
      [ "TP_Task_Data", "classTP__task__data_1_1TP__Task__Data.html", "classTP__task__data_1_1TP__Task__Data" ]
    ] ],
    [ "TP_task_imu", null, [
      [ "TP_Task_IMU", "classTP__task__imu_1_1TP__Task__IMU.html", "classTP__task__imu_1_1TP__Task__IMU" ]
    ] ],
    [ "TP_task_motor", null, [
      [ "TP_Task_Motor", "classTP__task__motor_1_1TP__Task__Motor.html", "classTP__task__motor_1_1TP__Task__Motor" ]
    ] ],
    [ "TP_task_touchpanel", null, [
      [ "TP_Task_TouchPanel", "classTP__task__touchpanel_1_1TP__Task__TouchPanel.html", "classTP__task__touchpanel_1_1TP__Task__TouchPanel" ]
    ] ],
    [ "TP_task_user", null, [
      [ "TP_Task_User", "classTP__task__user_1_1TP__Task__User.html", "classTP__task__user_1_1TP__Task__User" ]
    ] ],
    [ "TP_touchpanel", null, [
      [ "TP_TouchPanel", "classTP__touchpanel_1_1TP__TouchPanel.html", "classTP__touchpanel_1_1TP__TouchPanel" ]
    ] ]
];